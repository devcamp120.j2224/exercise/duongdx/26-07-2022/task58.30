package com.devcamp.s50.task5830.task58_30.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5830.task58_30.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer , Long >{
    
}
