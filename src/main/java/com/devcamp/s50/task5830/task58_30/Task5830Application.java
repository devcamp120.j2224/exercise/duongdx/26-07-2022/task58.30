package com.devcamp.s50.task5830.task58_30;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5830Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5830Application.class, args);
	}

}
