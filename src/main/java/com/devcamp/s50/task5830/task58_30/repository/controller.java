package com.devcamp.s50.task5830.task58_30.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.task5830.task58_30.model.CCustomer;

@CrossOrigin
@RequestMapping("/")
@RestController
public class controller {
    @Autowired
    ICustomerRepository iCustomerRepository ;
    @GetMapping("/customer")
    public ResponseEntity<List<CCustomer>> getAllCustomer(){
        try {
            List<CCustomer> listCustomer = new ArrayList<CCustomer>();

            iCustomerRepository.findAll().forEach(listCustomer::add);

            return new ResponseEntity<List<CCustomer>>(listCustomer, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
